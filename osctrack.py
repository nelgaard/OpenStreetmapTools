#!/usr/bin/env python2

# gpsbabel -t -i unicsv -o gpx t.csv x.gpx
# https://www.gpsbabel.org/htmldoc-development/fmt_unicsv.html
import sys
import argparse
import glob
import gzip
import time
from pexif import JpegFile
from pexif import Rational
from datetime import datetime

trackf = gzip.open(sys.argv[1], 'rt')
trackf.readline()

def ipol(tr1,tr2,t,l):
    return tr1[l]+(tr2[l]-tr1[l])*(t-tr1['t'])/(tr2['t']-tr1['t'])

time.timezone=0

tracks=[]
directions=[]
print("utc_d,utc_t,lat,lon,head")
for line in trackf.readlines():
    ls=line.split(";")
    if line!="DONE" and ls[1] and ls[2]:
        t=ls[0]
        lon=ls[1]
        lat=ls[2]
        d=ls[3]
        tm=time.gmtime(float(t))
        print ("{},{},{},{},{}".format(time.strftime('%Y-%m-%d',tm),time.strftime('%H:%M:%S',tm),float(lat),float(lon),float(d) ))
    elif line!="DONE" and ls[13]:
        t=ls[0]
        directions.append({'t':float(t),'direction':float(ls[13])})
