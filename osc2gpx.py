#!/usr/bin/env python3
import sys
import argparse
import glob
import gzip
import time
from datetime import datetime
import os

trackf = gzip.open("track.txt.gz", 'rt')
trackf.readline()
tracks=trackf.readlines()

for fl in os.listdir('.'):
    if fl[-4:] != ".mp4":
        continue
    fi=fl.split(".")[0]
    gpxfile=open('f'+str(fi)+'.gpx',mode="w",encoding='utf-8')
    tt=os.popen('ffprobe -print_format csv  -loglevel quiet -show_entries stream_tags=creation_time '+ fl).read().split(",")[1].replace("Z","").replace("\n","")
    starttime=datetime.fromisoformat(tt)
    tu=float(time.mktime(starttime.timetuple())) + 3600.0*2
    print(fi," starttime=",starttime, " tu=",tu)

    print(
        """<?xml version="1.0"?>
    <gpx version="1.0" creator="Viking -- http://viking.sf.net/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns="http://www.topografix.com/GPX/1/0"
    xsi:schemaLocation="http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd">
    <trk>
        <trkseg>
    """,file=gpxfile)
    for line in tracks:
        ls=line.split(";")
        if line!="DONE" and ls[1] and ls[2]:
            t=float(ls[0]) 
            #print("t=",t,"tu=",tu)
            if (t<tu):
                continue
            lon=ls[1]
            lat=ls[2]
            print('<trkpt lat="{:s}" lon="{:s}"><time>{:s}</time></trkpt>'.format(lat,lon,datetime.utcfromtimestamp(t).strftime('%Y-%m-%dT%H:%M:%S.%fZ')),file=gpxfile)

    print("""  </trkseg>
    </trk>
    </gpx>
    """,file=gpxfile)
    gpxfile.close()
