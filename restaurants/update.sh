#!/bin/bash
#  area["name"="Denmark"]->.sa;\
# {{geocodeArea:\"Denmark\"}}->.sa;\
#  area["name"="Denmark"]->.sa;\
date
q="[timeout:2600] [out:json];\
( \
  area(3600050046)->.sa;\
  node["amenity"="restaurant"](area.sa);\
  way["amenity"="restaurant"](area.sa);\
  node["amenity"="ice_cream"](area.sa);\
  node["amenity"="community_centre"](area.sa);\
  way["amenity"="community_centre"](area.sa);\
  node["amenity"="garden_centre"](area.sa);\
  way["amenity"="garden_centre"](area.sa);\
  node["amenity"="kitchen"](area.sa);\
  node["amenity"="hospital"](area.sa);\
  way["amenity"="hospital"](area.sa);\
  node["amenity"="clinic"](area.sa);\
  node["amenity"="pharmacy"](area.sa);\
  node["amenity"="events_venue"](area.sa);\
  node["amenity"="theatre"](area.sa);\
  way["amenity"="events_venue"](area.sa);\
  node["amenity"="cafe"](area.sa);\
  node(area.sa)["amenity"="fast_food"];\
  way(area.sa)["amenity"="fast_food"];\
  node["amenity"="bar"](area.sa);\
  node["amenity"="pub"](area.sa);\
  node["amenity"="cinema"](area.sa);\
  way["amenity"="cinema"](area.sa);\
  node["amenity"="smokehouse"](area.sa);\
  node["shop"="farm"](area.sa);\
  way["shop"="farm"](area.sa);\
  node["shop"="craft"]["craft"="catering"](area.sa);\
  node["shop"="butcher"](area.sa);\
  node["shop"="bakery"](area.sa);\
  node["shop"="pastry"](area.sa);\
  node["shop"="craft"](area.sa);\
  node["shop"="florist"](area.sa);\
  node["shop"="kiosk"](area.sa);\
  node["shop"="houseware"](area.sa);\
  node["shop"="electronics"](area.sa);\
  node["shop"="bicycle"](area.sa);\
  node["shop"="clothes"](area.sa);\
  node["shop"="alcohol"](area.sa);\
  node["shop"="wine"](area.sa);\
  node["shop"="supermarket"](area.sa);\
  way["shop"="supermarket"](area.sa);\
  node["shop"="party"](area.sa);\
  node["shop"="nutrition_supplements"](area.sa);\
  node["shop"="convenience"](area.sa);\
  node["shop"="confectionery"](area.sa);\
  node["shop"="cheese"](area.sa);\
  node["shop"="chemist"](area.sa);\
  node["shop"="variety_store"](area.sa);\
  node["department"~\".\"](area.sa);\
  node["shop"="garden_centre"](area.sa);\
  way["shop"="garden_centre"](area.sa);\
  node["shop"="doityourself"](area.sa);\
  way["shop"="doityourself"](area.sa);\
  node["leisure"="golf_course"](area.sa);\
  way["leisure"="golf_course"](area.sa);\
  node["leisure"="sports_centre"](area.sa);\
  node["leisure"="fitness_centre"](area.sa);\
  node["leisure"="fishing"](area.sa);\
  way["leisure"="fitness_centre"](area.sa);\
  node["leisure"="water_park"](area.sa);\
  node["tourism"="hotel"](area.sa);\
  node["amenity"="hookah_lounge"](area.sa);\
  node["tourism"="guest_housel"](area.sa);\
  way["tourism"="hotel"](area.sa);\
  node["tourism"="zoo"](area.sa);\
  way["tourism"="zoo"](area.sa);\
  node["tourism"="museum"](area.sa);\
  node["office"="company"](area.sa);\
  way["tourism"="museum"](area.sa);\
  node["tourism"="guest_house"](area.sa);\
  node["tourism"="camp_site"](area.sa);\
  way["tourism"="camp_site"](area.sa);\
  node["tourism"="hostel"](area.sa);\
  way["tourism"="hostel"](area.sa);\
  way["club"="sport"](area.sa);\
  node["club"="sport"](area.sa);\
  node[\"fvst:navnelbnr\" ~ \".\"]( 54.395, 3.853, 57.8321, 16.9097 );\
  way[\"fvst:navnelbnr\" ~ \".\"]( 54.395, 3.853, 57.8321, 16.9097 );\
  relation[\"fvst:navnelbnr\" ~ \".\"](area.sa);\
);\
out center meta;\
"

#echo "$q"

opi=http://overpass-api.de/api/interpreter
#opi=https://lz4.overpass-api.de/api/interpreter
#opi=https://z.overpass-api.de/api/interpreter
#opi=https://overpass.osm.ch/api/interpreter

#opi=https://overpass.kumi.systems/api/interpreter
#opi=https://overpass.openstreetmap.fr/api/interpreter

if [[ x$1 != "xskiposm" && x$2 != "xskiposm" ]] ; then
    echo get osm data from $opi
        if curl -o data/osmres.json -G --connect-timeout 140 --silent --data-urlencode  "data=$q" $opi; then
    #   if curl -o data/osmres.json -G --connect-timeout 140 --data-urlencode  "data=$q" $opi; then
        osize=$(stat -c%s data/osmres.json)
        echo size $osize
        if [ "$osize" -gt "9999" ]; then
            echo got OSM
        else
            echo OSM file too small
            exit 1
        fi
    else
        echo osm download failed
        exit 1
    fi
fi

echo get kontrolresultater
## http://www.findsmiley.dk/xml/allekontrolresultater.xml
buf=data/allekontrolresultater.xml.bu$(date "+%d")
cp data/allekontrolresultater.xml $buf
gzip -f $buf
#if wget -O data/allekontrolresultater.xml --timeout 40 --quiet --timestamping  https://www.foedevarestyrelsen.dk/_layouts/15/sdata/smiley_xml.xml; then
if curl   -o data/allekontrolresultater.xml --max-filesize 200M --connect-timeout 140 --silent --show-error https://www.foedevarestyrelsen.dk/Media/638212360788086849/Smiley_xml.xml; then
    echo got kontrolresultater
#    xsltproc smilres.xslt data/allekontrolresultater.xml | sed -e "s/\t/ /" | sed -e 's/\\//'> data/r.json
    xsltproc smilresfull.xslt data/allekontrolresultater.xml | sed -e "s/\t/ /" | sed -e 's/\\//' > data/rfull.json
    xsltproc smilresno.xslt data/allekontrolresultater.xml | sed -e "s/\t/ /" | sed -e 's/\\//' > data/rall.json
else
    echo did not get kontrolresultater
fi
echo find matches and misses
python3 missingRest.py
#python3 cvr.py

if [[ x$1 != "xskipaddr" && x$2 != "xskipaddr" ]] ; then
  echo look up addrs
  ./addressLookup.py all > addr.log
  tail -3 addr.log
  python3 missingRest.py
fi

