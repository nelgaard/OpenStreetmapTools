#!/usr/bin/python3
import json
from pprint import pprint
mss=json.loads(open("ms.json",'r', encoding='utf-8').read())["elements"]
hws=["motorway","trunk","primary","secondary","tertiary","unclassified","residential"]

s={}
st={}
for ms in mss:
    if ms["type"]=="missing":
        s[ms["tags"]["highway"]]=ms["tags"]["hwlen"]
    elif ms["type"]=="total":
        st[ms["tags"]["highway"]]=ms["tags"]["hwlen"]

for hw in hws:
    print(hw,":: missing maxspeed:",round(float(s[hw])),", total:",round(float(st[hw])),", pct with maxspeed:",round(100-100*float(s[hw])/float(st[hw]),2))
