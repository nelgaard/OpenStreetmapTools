#!/usr/bin/env python3
import sys
import argparse
import re
import overpy
import json

op = overpy.Overpass()
winemhs=op.query('nwr[tourism=caravan_site]["caravan_site:type"=winery];out center;')
wh={"features":[],"type" : "FeatureCollection"}
sname = re.compile(r"(wohn|reise)mobil(.?stellplatz)?|(accueil|aire|stationnement).?(de)?(.camping.car.?)?", re.IGNORECASE)

for winemh in winemhs.nodes:
    d=""
    for p,v in winemh.tags.items():
        ps=p.replace("contact:","")
        sn=re.sub(sname,"",winemh.tags.get("name","undef"))
        if ps in ["phone","capacity","power_supply","sanitary_dump_station","water_point","fee","description","toilets","product","shower"]:
            if d!="":
                d += "\n"    
            d += ps+":"+v
    wh["features"].append({"type":"Feature","geometry": {"type": "Point","coordinates": [float(winemh.lon),float(winemh.lat)]},"properties":{"name":sn,"description":d}})
print(json.dumps(wh,indent=2))

